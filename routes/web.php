<?php


use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\PartnerController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\Public\SectionController as PublicSection;
use App\Http\Controllers\Public\ArticleController as PublicArticle;
use App\Http\Controllers\Public\EventController as PublicEvent;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => '/'], function() {
    Route::get('', [MainController::class, 'index'])->name('index');

    Route::get('authorize', [AuthController::class, 'showAuth'])->name('authorize');
    Route::post('authorize', [AuthController::class, 'auth'])->name('authorize.post');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    Route::group(['prefix' => 'section'], function() {
//        FIXME: подумать ещё раз над URL
        Route::get('{sectionAlias}/{subsectionAlias}/articles/{articleAlias}', [PublicArticle::class, 'getSubsectionArticle'])->name('get-subsection-article');
        Route::get('{sectionAlias}/articles/{articleAlias}', [PublicArticle::class, 'getSectionArticle'])->name('get-section-article');
        Route::get('{sectionAlias}/{subsectionAlias}', [PublicSection::class, 'getSubsection'])->name('get-subsection');
        Route::get('{sectionAlias}', [PublicSection::class, 'getSection'])->name('get-section');
    });

    Route::group(['prefix' => 'events'], function() {
        Route::get('today', [PublicEvent::class, 'getTodayEvents'])->name('today-events');
        Route::get('coming-soon', [PublicEvent::class, 'getComingSoonEvents'])->name('coming-son-events');
        Route::get('off-site', [PublicEvent::class, 'getOffsiteEvents'])->name('off-site-events');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'role'], function() {
        Route::get('', [AdminController::class, 'index'])->name('admin-dashboard');
        Route::resource('users', UserController::class);
        Route::resource('partners', PartnerController::class);
        Route::resource('banners', BannerController::class);
        Route::resource('sections', SectionController::class);
        Route::resource('articles', ArticleController::class);
        Route::resource('events', EventController::class);
        Route::get('images', [ImageController::class, 'getImages'])->name('images');
        Route::post('images/{image}', [ImageController::class, 'deleteImage'])->name('delete-image');
        Route::get('images/{id}/update', [ImageController::class, 'getEditImage'])->name('update-image');
        Route::post('images/{id}/update', [ImageController::class, 'editImage'])->name('update-image-post');
        Route::post('images/store', [ImageController::class, 'store'])->name('images.store');
    });
});
