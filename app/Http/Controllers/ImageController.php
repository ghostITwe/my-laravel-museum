<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageRequestController;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

//    FIXME: доделать картинки, чтобы работало всё
    public function store(Request $request) {
//      return response()->json([
////            'url' =>
////        ]);
    }

    public function getImages(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $images = Image::query();

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $images = $images->orderBy($currentParams[0], $currentParams[1]);
        }

        $images = $images->get();

        return view('admin_layouts.image.images', [
            'images' => $images
        ]);
    }

    public function getEditImage($id)
    {
        $image = Image::find($id);

        return view('admin_layouts.image.edit_image', [
            'image' => $image
        ]);
    }

    public function editImage(ImageRequestController $request, $id)
    {
        $post = $request->validated();

        $image = Image::find($id);
        $image->alt = $post['alt'];
        if (isset($post['image'])) {
            $image->path = self::updateImage($image->path, $post['image']);
        }
        $image->save();

        return redirect($post['url']);
    }

    public static function getUploadImagePath(string $model, $image) {
        $fileName = explode('.', $image->getClientOriginalName());
        $fileType = array_pop($fileName);
        $fileName = md5(implode($fileName) . time()) . '.' . $fileType;
        return $image->storeAs($model, $fileName);
    }

    public static function createImage(string $model, $image, $alt) {
        $newImage = new Image();
        $newImage->path = Storage::url(self::getUploadImagePath($model, $image));
        $newImage->alt = $alt;
        $newImage->save();

        return $newImage->id;
    }

    public static function deleteImage($image) {
        $deleteImage = Image::find($image->id);
        Storage::delete(self::imageUrlToString($deleteImage->path));
        $deleteImage->delete();
    }

    public static function updateImage($path, $newImage): string
    {
        $oldPath = explode('/', self::imageUrlToString($path));
        $fileName = array_pop($oldPath);
        $oldPath = implode('/', $oldPath);
        return Storage::url($newImage->storeAs($oldPath, $fileName));
    }

    private static function imageUrlToString($path) {
        return implode('/', array_slice(explode('/', $path), 4));
    }
}
