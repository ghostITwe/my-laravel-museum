<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Partner;
use App\Models\Section;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $sections = Section::with([
            'image',
            'articles'
        ])->whereNull('parent_id')->get();
        $banners = Banner::with('image')->get();
        $partners = Partner::query()->get();

        return view('public.index-template', [
            'sections' => $sections,
            'banners' => $banners,
            'partners' => $partners
        ]);
    }
}
