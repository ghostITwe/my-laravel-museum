<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function getSection($alias)
    {
        $section = Section::with([
            'image',
            'articles',
            'children'
        ])
            ->where('alias', $alias)
            ->first();

        return view('public.section', [
            'section' => $section
        ]);
    }

    public function getSubsection($sectionAlias, $subsectionAlias)
    {
        $subsection = Section::with([
            'image',
            'articles'
        ])
            ->where('alias', $subsectionAlias)
            ->first();

        return view('public.subsection', [
            'subsection' => $subsection,
            'sectionAlias' => $sectionAlias
        ]);
    }
}
