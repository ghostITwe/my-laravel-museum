<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\Event;
use DateTime;

class EventController extends Controller
{
    public function getTodayEvents() {
        $startDate = new DateTime();
        $startDate->setTime(0,0);
        $endDate = new DateTime();
        $endDate->setTime(23,59, 59);

        $events = Event::with([
            'image'
        ])
            ->where('type','=', 'Не передвижная выставка')
            ->whereDate('start', $startDate)
            ->whereDate('end', $endDate)
            ->get();

        return view('public.events', [
            'events' => $events
        ]);
    }

    public function getComingSoonEvents() {

        $endDate = new DateTime();
        $endDate->setTime(23,59,59);

        $events = Event::with([
            'image'
        ])
            ->where('type','=', 'Не передвижная выставка')
            ->whereDate('end', '>', $endDate)
            ->get();

        return view('public.events', [
            'events' => $events
        ]);
    }

    public function getOffsiteEvents() {
        $events = Event::with([
            'image'
        ])
            ->where('type', '=', 'Передвижная выставка')
            ->get();

        return view('public.events', [
            'events' => $events
        ]);
    }
}
