<?php

namespace App\Http\Controllers\Public;

use App\Http\Controllers\Controller;
use App\Models\Article;

class ArticleController extends Controller
{
    public function getSectionArticle($sectionAlias, $articleAlias) {
        $article = Article::with([
            'image',
            'section'
        ])
            ->where('alias', $articleAlias)
            ->first();

        return view('public.article', [
            'article' => $article
        ]);
    }

    public function getSubsectionArticle($sectionAlias, $subsectionAlias, $articleAlias) {
        $article = Article::with([
            'image',
            'section'
        ])
            ->where('alias', $articleAlias)
            ->first();

        return view('public.article', [
            'article' => $article
        ]);
    }
}
