<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    private Request $request;
    const AUTH_RULES = [
        'login' => "required|string",
        'password' => "required"
    ];

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function showAuth() {
        if (Auth::check()) {
            return redirect()->route('index');
        } else {
            return view('authorize');
        }
    }

    public function auth() {
        if (Auth::check()) return redirect()->route('index');

        $validator = $this->checkAuthCredentials();

        if ($this->checkAuthCredentials()->fails()) {
            back()->withErrors($validator);
        }

        $credentials = $this->request->only('login', 'password');
        if (Auth::attempt($credentials)) {
            $this->request->session()->regenerate();
            return redirect()->route('index');
        }

        return back()->withErrors([
            'not_match' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout() {
        Auth::logout();
        return redirect()->route('index');
    }

    private function checkAuthCredentials() {
        return Validator::make($this->request->all(), static::AUTH_RULES);
    }
}
