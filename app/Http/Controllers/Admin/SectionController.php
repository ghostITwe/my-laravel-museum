<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\Section\CreateSectionRequestController;
use App\Http\Requests\Section\UpdateSectionRequestController;
use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $sections = Section::query()->with([
            'image',
            'parent'
        ]);

        if (isset($params['search']) && !empty($params['search'])) {
            $sections = $sections->whereRaw("MATCH(title,description) AGAINST(? IN BOOLEAN MODE)", [$params['search']]);
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $sections = $sections->orderBy($currentParams[0], $currentParams[1]);
        }

        $sections = $sections->get();

        return view('admin_layouts.section.sections', [
            'sections' => $sections
        ]);
    }

    public function create()
    {
        $sections = Section::whereNull('parent_id')->get();

        return view('admin_layouts.section.create_section', [
            'sections' => $sections
        ]);
    }

    public function store(CreateSectionRequestController $request)
    {
        $post = $request->validated();

        $section = new Section();
        $section->title = $post['title'];
        $section->description = $post['description'];
        $section->parent_id = $post['parent_id'] == "null" ? null : $post['parent_id'];
        $section->alias = $post['alias'];
        $section->image_id = ImageController::createImage('sections', $post['image'], $post['alt']);
        $section->save();

        return redirect()->route('sections.index');
    }

    public function edit(Section $section)
    {
        $sections = Section::whereNull('parent_id')->get();
        $editSection = Section::find($section->id);

        return view('admin_layouts.section.edit_section', [
            'section' => $editSection,
            'sections' => $sections
        ]);
    }

    public function update(UpdateSectionRequestController $request, Section $section)
    {
        $post = $request->validated();

        $updateSection = Section::with('image')->find($section->id);

        if (isset($post['image'])) {
            ImageController::updateImage($updateSection->image->path, $post['image']);
        }

        $updateSection->title = $post['title'];
        $updateSection->description = $post['description'];
        $updateSection->parent_id = $post['parent_id'] == "null" ? null : $post['parent_id'];
        $updateSection->alias = $post['alias'];
        $updateSection->image()->update([
            'alt' => $post['alt']
        ]);
        $updateSection->save();

        return redirect()->route('sections.index');
    }

    public function destroy(Section $section)
    {
        $deleteSection = Section::find($section->id);
        ImageController::deleteImage($deleteSection->image);
        $deleteSection->delete();

        return redirect()->route('sections.index');
    }
}
