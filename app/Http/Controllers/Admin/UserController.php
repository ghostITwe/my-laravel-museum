<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\CreateUserRequestController;
use App\Http\Requests\User\UpdateUserRequestController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $users = User::query()->where('role', 'moderator');

        if (isset($params['search']) && !empty($params['search'])) {
            $users = $users->where(function ($query) use ($params) {
                $query->where('email', 'like', '%' . $params['search'] . '%')
                    ->orWhere('login', 'like', '%' . $params['search'] . '%');
            });
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $users = $users->orderBy($currentParams[0], $currentParams[1]);
        }

        $users = $users->get();

        return view('admin_layouts.user.users', [
            'users' => $users
        ]);
    }


    public function create()
    {
        return view('admin_layouts.user.create_user');
    }


    public function store(CreateUserRequestController $request)
    {
        $post = $request->validated();

        $user = new User();
        $user->email = $post['email'];
        $user->login = $post['login'];
        $user->role = $post['role'];
        $user->password = Hash::make($post['password']);
        $user->save();

        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        $userEdit = User::findOrFail($user->id);

        return view('admin_layouts.user.edit_user', [
            'user' => $userEdit
        ]);
    }

    public function update(UpdateUserRequestController $request, User $user)
    {
        $post = $request->validated();

        $updateUser = User::findOrFail($user->id);
        $updateUser->email = $post['email'];
        $updateUser->login = $post['login'];
        $updateUser->role = $post['role'];
        $updateUser->save();

        return redirect()->route('users.index');
    }

    public function destroy(User $user)
    {
        $deleteUser = User::findOrFail($user->id);
        $deleteUser->delete();

        return redirect()->route('users.index');
    }
}
