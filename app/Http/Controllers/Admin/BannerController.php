<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\Banner\CreateBannerRequestController;
use App\Http\Requests\Banner\UpdateBannerRequestController;
use App\Models\Banner;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $banners = Banner::query()->with('image');

        if (isset($params['search']) && !empty($params['search'])) {
            $banners = $banners->where('name', 'like', '%' . $params['search'] . '%');
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $banners = $banners->orderBy($currentParams[0], $currentParams[1]);
        }

        $banners = $banners->get();


        return view('admin_layouts.banner.banners', [
            'banners' => $banners
        ]);
    }

    public function create()
    {
        return view('admin_layouts.banner.create_banner');
    }

    public function store(CreateBannerRequestController $request)
    {
        $post = $request->validated();

        $banner = new Banner();
        $banner->name = $post['name'];
        $banner->link = $post['link'];
        $banner->image_id = ImageController::createImage('banners', $post['image'], $post['alt']);
        $banner->save();

        return redirect()->route('banners.index');
    }

    public function show(Banner $banner)
    {
        $bannerShow = Banner::find($banner->id);

        return view('banner', [
            'banner' => $bannerShow
        ]);
    }

    public function edit(Banner $banner)
    {
        $bannerEdit = Banner::with('image')->findOrFail($banner->id);

        return view('admin_layouts.banner.edit_banner', [
            'banner' => $bannerEdit
        ]);
    }

    public function update(UpdateBannerRequestController $request, Banner $banner)
    {
        $post = $request->validated();

        $updateBanner = Banner::with('image')->find($banner->id);
        if (isset($post['image'])) {
            ImageController::updateImage($updateBanner->image->path, $post['image']);
        }
        $updateBanner->name = $post['name'];
        $updateBanner->link = $post['link'];
        $updateBanner->image()->update([
            'alt' => $post['alt']
        ]);
        $updateBanner->save();

        return redirect()->route('banners.index');
    }

    public function destroy(Banner $banner)
    {
        $deleteBanner = Banner::find($banner->id);
        ImageController::deleteImage($deleteBanner->image);
        $deleteBanner->delete();

        return redirect()->route('banners.index');
    }
}
