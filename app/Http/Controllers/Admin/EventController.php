<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\Event\CreateEventRequestController;
use App\Http\Requests\Event\UpdateEventRequestController;
use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $events = Event::query()->with('image');

        if (isset($params['search']) && !empty($params['search'])) {
            $events = $events->whereRaw("MATCH(title,description,annotation) AGAINST(? IN BOOLEAN MODE)", [$params['search']]);
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $events = $events->orderBy($currentParams[0], $currentParams[1]);
        }

        $events = $events->get();

        return view('admin_layouts.event.events', [
            'events' => $events
        ]);
    }

    public function create()
    {
        return view('admin_layouts.event.create_event');
    }

    public function store(CreateEventRequestController $request)
    {
        $post = $request->validated();

        $event = new Event();
        $event->title = $post['title'];
        $event->description = $post['description'];
        $event->annotation = $post['annotation'];
        $event->start = $post['start'];
        $event->end = $post['end'];
        $event->type = $post['type'];
        $event->alias = $post['alias'];
        $event->image_id = ImageController::createImage('events', $post['image'], $post['alt']);
        $event->save();

        return redirect()->route('events.index');
    }

    public function edit(Event $event)
    {
        $editEvent = Event::with('image')->find($event->id);

        return view('admin_layouts.event.edit_event', [
            'event' => $editEvent
        ]);
    }

    public function update(UpdateEventRequestController $request, Event $event)
    {
        $post = $request->validated();

        $updateEvent = Event::with('image')->find($event->id);

        if (isset($post['image'])) {
            ImageController::updateImage($updateEvent->image->path, $post['image']);
        }
        $updateEvent->title = $post['title'];
        $updateEvent->description = $post['description'];
        $updateEvent->annotation = $post['annotation'];
        $updateEvent->start = $post['start'];
        $updateEvent->end = $post['end'];
        $updateEvent->type = $post['type'];
        $updateEvent->alias = $post['alias'];
        $updateEvent->image()->update([
            'alt' => $post['alt']
        ]);
        $updateEvent->save();

        return redirect()->route('event.index');
    }


    public function destroy(Event $event)
    {
        $deleteEvent = Event::find($event->id);
        ImageController::deleteImage($deleteEvent->image);
        $deleteEvent->delete();

        return redirect()->route('events.index');
    }
}
