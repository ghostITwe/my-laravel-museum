<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Partner\PartnerRequestController;
use App\Models\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {

        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $partners = Partner::query();

        if (isset($params['search']) && !empty($params['search'])) {
            $partners = $partners->where('name', 'like', '%' . $params['search'] . '%');
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $partners = $partners->orderBy($currentParams[0], $currentParams[1]);
        }

        $partners = $partners->get();

        return view('admin_layouts.partner.partners', [
            'partners' => $partners
        ]);
    }


    public function create()
    {
        return view('admin_layouts.partner.create_partner');
    }


    public function store(PartnerRequestController $request)
    {
        $post = $request->validated();

        $partner = new Partner();
        $partner->name = $post['name'];
        $partner->link = $post['link'];
        $partner->save();

        return redirect()->route('partners.index');
    }

    public function edit(Partner $partner)
    {
        $partnerEdit = Partner::find($partner->id);

        return view('admin_layouts.partner.edit_partner', [
            'partner' => $partnerEdit
        ]);
    }


    public function update(PartnerRequestController $request, Partner $partner)
    {
        $post = $request->validated();

        $updatePartner = Partner::find($partner->id);
        $updatePartner->name = $post['name'];
        $updatePartner->link = $post['link'];
        $updatePartner->save();

        return redirect()->route('partners.index');
    }

    public function destroy(Partner $partner)
    {
        $deletePartner = Partner::find($partner->id);
        $deletePartner->delete();

        return redirect()->route('partners.index');
    }
}
