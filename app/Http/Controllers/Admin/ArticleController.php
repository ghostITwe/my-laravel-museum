<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\Article\CreateArticleRequestController;
use App\Http\Requests\Article\UpdateArticleRequestController;
use App\Models\Article;
use App\Models\Section;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    private $sorting;

    public function __construct() {
        $this->sorting = [
            'asc' => ['id', 'asc'],
            'desc' => ['id', 'desc']
        ];
    }

    public function index(Request $request)
    {
        $params = $request->validate([
            'search' => 'sometimes',
            'sort' => 'sometimes'
        ]);

        $articles = Article::query()->with([
            'image',
            'section'
        ]);

        if (isset($params['search']) && !empty($params['search'])) {
            $articles = $articles->whereRaw("MATCH(title,description,annotation) AGAINST(? IN BOOLEAN MODE)", [$params['search']]);
        }

        if (isset($params['sort']) && !empty($params['sort'])) {
            $currentParams = $this->sorting[$params['sort']];
            $articles = $articles->orderBy($currentParams[0], $currentParams[1]);
        }

        $articles = $articles->get();

        return view('admin_layouts.article.articles', [
            'articles' => $articles,
        ]);
    }

    public function create()
    {
        $sections = Section::query()->get();

        return view('admin_layouts.article.create_article', [
            'sections' => $sections
        ]);
    }

    public function store(CreateArticleRequestController $request)
    {
        $post = $request->validated();

        $article = new Article();
        $article->title = $post['title'];
        $article->description = $post['description'];
        $article->annotation = $post['annotation'];
        $article->image_id = ImageController::createImage('articles', $post['image'], $post['alt']);
        $article->section_id = $post['section'];
        $article->alias = $post['alias'];
        $article->save();

        return redirect()->route('articles.index');
    }

    public function show(Article $article)
    {
        //
    }

    public function edit(Article $article)
    {
        $editArticle = Article::with([
            'image',
            'section'
        ])->find($article->id);

        $sections = Section::query()->get();

        return view('admin_layouts.article.edit_article', [
            'article' => $editArticle,
            'sections' => $sections
        ]);
    }

    public function update(UpdateArticleRequestController $request, Article $article)
    {
        $post = $request->validated();

        $updateArticle = Article::with('image')->find($article->id);

        if (isset($post['image'])) {
            ImageController::updateImage($updateArticle->image->path, $post['image']);
        }

        $updateArticle->title = $post['title'];
        $updateArticle->description = $post['description'];
        $updateArticle->annotation = $post['annotation'];
        $updateArticle->section_id = $post['section'];
        $updateArticle->alias = $post['alias'];
        $updateArticle->image()->update([
            'alt' => $post['alt']
        ]);
        $updateArticle->save();

        return redirect()->route('articles.index');
    }

    public function destroy(Article $article)
    {
        $deleteArticle = Article::find($article->id);
        ImageController::deleteImage($deleteArticle->image);
        $deleteArticle->delete();

        return redirect()->route('articles.index');
    }
}
