<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'sometimes|email',
            'login' => 'sometimes|string|min:6',
            'password' => 'sometimes|confirmed',
            'role' => 'sometimes'
        ];
    }

    public function attributes()
    {
        return [
            'email' => 'Почта',
            'login' => 'Логин',
            'password' => 'Пароль',
            'role' => 'Роль'
        ];
    }
}
