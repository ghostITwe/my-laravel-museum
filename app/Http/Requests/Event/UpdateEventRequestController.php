<?php

namespace App\Http\Requests\Event;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEventRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'start' => 'required|date',
            'end' => 'required|date',
            'type' => 'required',
            'image' => 'sometimes|image',
            'alt' => 'sometimes|required_with:image',
            'alias' => 'required|unique:events,alias',
            'annotation' => 'sometimes'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Название',
            'annotation' => 'Аннотация',
            'start' => 'Дата начала',
            'end' => 'Дата окончания',
            'type' => 'Тип события',
            'image' => 'Обложка',
            'alt' => 'Альтернативное название обложки',
            'alias' => 'Псевдоним',
            'description' => 'Описание'
        ];
    }
}
