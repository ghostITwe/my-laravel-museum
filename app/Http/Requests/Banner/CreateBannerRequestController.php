<?php

namespace App\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class CreateBannerRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'link' => 'required|string',
            'alt' => 'required|string',
            'image' => 'required|image'
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Название',
            'link' => 'ссылка на ресурс',
            'image' => 'обложка баннера',
            'alt' => 'альтернативное название обложки баннера'
        ];
    }
}
