<?php

namespace App\Http\Requests\Banner;

use Illuminate\Foundation\Http\FormRequest;

class UpdateBannerRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|string',
            'link' => 'required|string',
            'image' => 'sometimes|image',
            'alt' => 'required_with:image|string',
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Название',
            'link' => 'ссылка на ресурс',
            'image' => 'обложка баннера',
            'alt' => 'альтернативное название обложки баннера'
        ];
    }
}
