<?php

namespace App\Http\Requests\Article;

use Illuminate\Foundation\Http\FormRequest;

class UpdateArticleRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'section' => 'required',
            'description' => 'required|string',
            'alt' => 'required|string',
            'image' => 'sometimes|image',
            'alias' => 'required|unique:events,alias',
            'annotation' => 'sometimes'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Название',
            'section' => 'Секция',
            'description' => 'Описание',
            'image' => 'Обложка',
            'alt' => 'Альтернативное название обложки',
            'alias' => 'Псевдоним',
            'annotation' => 'Аннотация'
        ];
    }
}
