<?php

namespace App\Http\Requests\Section;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSectionRequestController extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required|string',
            'parent_id' => 'sometimes',
            'description' => 'required|string',
            'image' => 'sometimes|image',
            'alt' => 'sometimes|required_with:image',
            'alias' => 'required|unique:events,alias'
        ];
    }

    public function attributes()
    {
        return [
            'title' => 'Название',
            'parent_id' => 'подсекция',
            'description' => 'описание секции',
            'image' => 'обложка секции',
            'alt' => 'альтернативное название обложки секции',
            'alias' => 'Псевдоним'
        ];
    }
}
