<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    public function banner() {
        return $this->hasOne(Banner::class);
    }

    public function event() {
        return $this->hasOne(Event::class);
    }

    public function section() {
        return $this->hasOne(Section::class);
    }

    public function article() {
        return $this->hasOne(Article::class);
    }
}
