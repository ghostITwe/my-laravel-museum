<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    use HasFactory;

    public function articles() {
        return $this->hasMany(Article::class);
    }

    public function image() {
        return $this->belongsTo(Image::class);
    }

    public function parent() {
        return $this->belongsTo(Section::class, 'parent_id');
    }

    public function children() {
        return $this->hasMany(Section::class, 'parent_id');
    }
}
