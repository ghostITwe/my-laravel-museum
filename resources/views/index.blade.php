<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="bg-light-accent flex flex-col min-h-screen">
        <header class="bg-accent text-white shadow-lg p-2">
            <nav class="flex flex-wrap gap-4">
                @if(Auth::check())
                    <a href="{{ route('admin-dashboard') }}" class="link">Admin</a>
                @endif
                <a href="#" class="link">lorem</a>
                <a href="#" class="link">lorem</a>
            </nav>
        </header>
        <main class="flex-1 body-font grid content-start gap-4 p-4 sm:p-8">
            @yield('content')
{{--        <section class="text-gray-700 body-font">
                <article class="p-4 flex flex-col justify-center items-center max-w-sm mx-auto">
                    <picture class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center" style="background-image: url(https://images.unsplash.com/photo-1590608897129-79da98d15969?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1951&q=80);"></picture>

                    <a-info class="lg:w-80 flex flex-col items-baseline gap-1.5 -mt-10 overflow-hidden p-5 card">
                        <header class="inline-flex flex-wrap items-center gap-1.5">
                            <dot class="h-4 w-4 rounded-full bg-light-secondary">
                                <dot class="block h-2 w-2 rounded-full m-1 bg-secondary"></dot>
                            </dot>
                            <a href="#" class="text-sm">Секция</a>
                            <a href="#" class="text-sm">Подсекция</a>
                        </header>
                        <h3 class="font-medium text-2xl">Mon titre</h3>
                        <p class="text-justify">
                            Lorem ipsum dolor sit amet consectetur adipisicing elit.
                            Perspiciatis veritatis vel suscipit ex dolore possimus iure.
                        </p>
                        <a href="#" class="btn btn-accent">Подробнее</a>
                    </a-info>
                </article>
            </section>--}}

        </main>
        <footer class="bg-accent text-white p-2 grid gap-4">
            @yield('footer')
            <span>© 2021</span>
        </footer>
    </body>
</html>
