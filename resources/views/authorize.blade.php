<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Авторизация</title>
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('authorize.post') }}" method="post">
            <div class="grid gap-2">
                <h1 class="text-2xl sm:text-4xl text-center">Авторизация</h1>
                <span class="line"></span>
            </div>
            @csrf
            <div class="grid gap-1">
                <label for="login">Логин</label>
                <input class="input" type="text" name="login" id="login" placeholder="Логин">
            </div>
            <div class="grid gap-1">
                <label for="password">Пароль</label>
                <input class="input" type="password" name="password" id="password" placeholder="Пароль">
            </div>
            <button class="btn btn-accent">Авторизоваться</button>
        </form>
    </body>
</html>
