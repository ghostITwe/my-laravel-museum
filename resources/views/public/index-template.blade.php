@extends('index')
@section('content')
    @forelse($sections as $section)
        <article class="flex flex-col justify-center items-center sm:max-w-sm">
            <picture class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center" style="background-image: url({{ $section->image->path }});"></picture>
            <a-info class="md:w-80 xl:w-72 w-full flex flex-col items-baseline gap-1.5 -mt-10 overflow-hidden p-5 card">
                <header class="inline-flex flex-wrap items-center gap-1.5">
                    <dot class="h-4 w-4 rounded-full bg-light-secondary">
                        <dot class="block h-2 w-2 rounded-full m-1 bg-secondary"></dot>
                    </dot>
                    <span class="text-sm">Секция</span>
                </header>
                <h3 class="font-medium text-2xl">{{ $section->title }}</h3>
                <p class="text-justify">
                    {{ $section->annotation }}
                </p>
                <a href="{{ route('get-section', ['sectionAlias' => $section->alias]) }}" class="btn btn-accent">Подробнее</a>
            </a-info>
        </article>
    @empty
        <div>Пусто</div>
    @endforelse
@endsection

@section('footer')
    @if (count($partners))
        <section class="flex flex-wrap gap-2">
            <h3 class="text-xl w-full">Партнеры</h3>
            @foreach($partners as $partner)
                <a class="link" href="{{ $partner->link }}">{{ $partner->name }}</a>
            @endforeach
        </section>
    @endif

    @if (count($banners))
        <section class="flex flex-wrap gap-2">
            <h3 class="text-xl w-full">Баннеры</h3>
            @foreach($banners as $banner)
                <img class="max-h-20" src="{{ $banner->image->path }}" alt="{{ $banner->image->alt }}">
            @endforeach
        </section>
    @endif
@endsection
