@extends('index')

@section('content')
    @forelse($subsection->articles as $article)
        <article class="flex flex-col justify-center items-center sm:max-w-sm">
            <picture class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center" style="background-image: url({{ $article->image->path }});"></picture>
            <a-info class="md:w-80 xl:w-72 w-full flex flex-col items-baseline gap-1.5 -mt-10 overflow-hidden p-5 card">
                <header class="inline-flex flex-wrap items-center gap-1.5">
                    <dot class="h-4 w-4 rounded-full bg-light-secondary">
                        <dot class="block h-2 w-2 rounded-full m-1 bg-secondary"></dot>
                    </dot>
                    <a href="#" class="text-sm">{{ $article->title }}</a>
                </header>
                <h3 class="font-medium text-2xl">{{ $article->title }}</h3>
                <p class="text-justify">
                    {{ $article->annotation }}
                </p>
                <a href="{{ route('get-subsection-article', ['sectionAlias' => $sectionAlias, 'subsectionAlias' => $subsection->alias, 'articleAlias' => $article->alias]) }}" class="btn btn-accent">Подробнее</a>
            </a-info>
        </article>
    @empty
        <div>Пусто</div>
    @endforelse
@endsection
