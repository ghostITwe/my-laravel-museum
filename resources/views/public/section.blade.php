@extends('index')

@section('content')
    <h2>Подсекции</h2>
    <section class="grid content-start sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        @forelse($section->children as $subsection)
            <article class="flex flex-col justify-center items-center sm:max-w-sm">
                <picture class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center" style="background-image: url({{ $subsection->image->path }});"></picture>
                <a-info class="md:w-80 xl:w-72 w-full flex flex-col items-baseline gap-1.5 -mt-10 overflow-hidden p-5 card">
                    <header class="inline-flex flex-wrap items-center gap-1.5">
                        <dot class="h-4 w-4 rounded-full bg-light-secondary">
                            <dot class="block h-2 w-2 rounded-full m-1 bg-secondary"></dot>
                        </dot>
                        <a href="#" class="text-sm">{{ $section->title }}</a>
                        <span class="text-sm">Подсекция</span>
                    </header>
                    <h3 class="font-medium text-2xl">{{ $subsection->title }}</h3>
                    <p class="text-justify">
                        {{ $subsection->annotation }}
                    </p>
                    <a href="{{ route('get-subsection', ['sectionAlias' => $section->alias, 'subsectionAlias' => $subsection->alias]) }}" class="btn btn-accent">Подробнее</a>
                </a-info>
            </article>
        @empty
            <div>Пусто</div>
        @endforelse
    </section>
    <h2>Статьи</h2>
    <section class="grid content-start sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 gap-4">
        @forelse($section->articles as $article)
            <article class="flex flex-col justify-center items-center sm:max-w-sm">
                <picture class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center" style="background-image: url({{ $article->image->path }});"></picture>
                <a-info class="md:w-80 xl:w-72 w-full flex flex-col items-baseline gap-1.5 -mt-10 overflow-hidden p-5 card">
                    <header class="inline-flex flex-wrap items-center gap-1.5">
                        <dot class="h-4 w-4 rounded-full bg-light-secondary">
                            <dot class="block h-2 w-2 rounded-full m-1 bg-secondary"></dot>
                        </dot>
                        <a href="#" class="text-sm">{{ $section->title }}</a>
                        <span class="text-sm">Статья</span>
                    </header>
                    <h3 class="font-medium text-2xl">{{ $article->title }}</h3>
                    <p class="text-justify">
                        {{ $article->annotation }}
                    </p>
                    <a href="{{ route('get-section-article', ['sectionAlias' => $section->alias, 'articleAlias' => $article->alias]) }}" class="btn btn-accent">Подробнее</a>
                </a-info>
            </article>
        @empty
            <div>Пусто</div>
        @endforelse
    </section>
@endsection
