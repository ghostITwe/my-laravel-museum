<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <title>Статья - {{ $article->title }}</title>
    </head>
    <body class="bg-light-accent flex flex-col gap-2 min-h-screen">
        <header class="bg-accent text-white shadow-lg p-2">
            <nav>
                @if(Auth::check())
                    <a href="{{ route('admin-dashboard') }}" class="link">Admin</a>
                @endif
                <a href="#" class="link">lorem</a>
                <a href="#" class="link">lorem</a>
            </nav>
        </header>
        <main class="article card flex-1 body-font grid content-start gap-4 p-4 sm:p-8 container lg:max-w-screen-lg mx-auto">
            <header class="grid gap-4">
                <a-info class="grid gap-2">
                    <h1 class="text-4xl">{{ $article->title }}</h1>
                    <p>{{ $article->annotation }}</p>
                    <time class="text-sm text-gray-700">{{ $article->created_at->format('d.m.Y H:m') }}</time>
                </a-info>
                <span class="line"></span>
                <img class="border-2 rounded-xl" src="{{ $article->image->path }}" alt="{{ $article->image->alt }}">
            </header>
            {!! $article->description !!}
        </main>
        <footer class="bg-accent text-white p-2">
            <span>© 2021</span>
        </footer>
    </body>
</html>
