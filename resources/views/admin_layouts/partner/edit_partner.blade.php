<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Изменение партнера</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('partners.update', ['partner' => $partner]) }}" method="post">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('partners.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Изменение партнера</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            @method('PUT')
            <f-field class="grid gap-1">
                <label for="login">Название</label>
                <input class="input" type="text" name="name" id="name" value="{{ old('name') ?? $partner->name }}" placeholder="Название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="password">Ссылка</label>
                <input class="input" type="text" name="link" id="link" value="{{ old('link') ?? $partner->link }}" placeholder="Ссылка">
            </f-field>
            <button class="btn btn-accent">Изменить</button>
        </form>
    </body>
</html>
