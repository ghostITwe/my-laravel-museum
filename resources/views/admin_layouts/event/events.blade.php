<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Админ панель - события</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="grid bg-light-accent text-sm sm:text-base">
        <header class="flex flex-col sm:flex-row flex-wrap items-center sm:justify-between gap-4 border-2 bg-white p-4 sm:p-8 shadow-lg">
            <a class="btn btn-accent p-2 flex gap-1" href="{{ route('admin-dashboard') }}">
                @include('layouts.svg.left-arrow')
            </a>
            <article class="grid gap-1.5">
                <h1 class="text-2xl sm:text-4xl">События</h1>
                <span class="line"></span>
            </article>
            <article>
                <form class="flex gap-2 m-0" action="{{ route('events.index') }}">
                    <input class="input" type="text" name="search" id="search" placeholder="Что ищем?">
                    <button class="btn btn-accent p-2">
                        @include('layouts.svg.search')
                    </button>
                </form>
            </article>
        </header>
        <main class="grid gap-4 p-4 sm:p-8">
            <nav class="flex gap-4">
                <a href="{{ route('events.create') }}" class="inline-block btn btn-accent p-2">
                    @include('layouts.svg.add')
                </a>
                <form class="flex gap-2 m-0" action="{{ route('events.index') }}">
                    <select name="sort" id="sort">
                        <option value="asc">Сотировать по возрастанию</option>
                        <option value="desc">Сотировать по убыванию</option>
                    </select>
                    <button class="btn btn-accent p-2">Применить</button>
                </form>
            </nav>
            <section class="grid gap-2">
                @forelse($events as $event)
                    <article class="grid sm:grid-cols-6 gap-2 p-2 sm:p-4 card">
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">ID</p>
                            <p class="text-base sm:text-xl">{{ $event->id }}</p>
                        </t-cell>
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">Название события</p>
                            <p class="text-base sm:text-xl ">{{ $event->title }}</p>
                        </t-cell>
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">Псевдоним</p>
                            <p class="text-base sm:text-xl ">{{ $event->alias }}</p>
                        </t-cell>
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">Дата начала события</p>
                            <p class="text-base sm:text-xl ">{{ $event->start }}</p>
                        </t-cell>
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">Дата окончания события</p>
                            <p class="text-base sm:text-xl ">{{ $event->end }}</p>
                        </t-cell>
                        <t-cell class="bg-gray-100 p-1 rounded border-2 hover:shadow-inner">
                            <p class="text-gray-500">ID фотографии</p>
                            <a class="text-base sm:text-xl link" href="{{ route('update-image', ['id' => $event->image->id]) }}">{{ $event->image->id }}</a>
                        </t-cell>
                        <nav class="col-span-full flex justify-end gap-1">
                            <a class="btn btn-accent p-2" href="{{ route("events.edit", ["event" => $event]) }}">
                                @include('layouts.svg.edit')
                            </a>
                            <form class="m-0 grid" action="{{ route("events.destroy", ["event" => $event]) }}" method="post">
                                @csrf
                                @method("DELETE")
                                <button class="btn btn-red p-2">
                                    @include('layouts.svg.delete')
                                </button>
                            </form>
                        </nav>
                    </article>
                @empty
                    <h2 class="text-2xl">Пусто</h2>
                @endforelse
            </section>
        </main>
    </body>
</html>
