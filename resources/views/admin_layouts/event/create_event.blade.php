<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Создание события</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('events.store') }}" method="post"
              enctype="multipart/form-data">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('events.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Создание события</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            <f-field class="grid gap-1">
                <label for="title">Название</label>
                <input class="input" type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Название события">
            </f-field>
            <f-field class="grid gap-1">
                <label for="alias">Псевдоним</label>
                <input class="input" type="text" name="alias" id="alias" value="{{ old('alias') }}" placeholder="Псевдоним">
            </f-field>
            <f-field class="grid gap-1">
                <label for="type">Тип события</label>
                <select name="type" id="type">
                    <option value="Передвижная выставка">Передвижная выставка</option>
                    <option value="Не передвижная выставка">Не передвижная выставка</option>
                </select>
            </f-field>
            <f-field class="grid gap-1">
                <label for="start">Дата начала события</label>
                <input class="input" type="datetime-local" name="start" value="{{ old('start') }}" id="start" placeholder="Дата начала события">
            </f-field>
            <f-field class="grid gap-1">
                <label for="end">Дата окончания события</label>
                <input class="input" type="datetime-local" name="end" id="end" value="{{ old('end') }}" placeholder="Дата окончания события">
            </f-field>
            <f-field class="grid gap-1">
                <label for="alt">Альтернативное название фотографии</label>
                <input class="input" type="text" name="alt" id="alt" value="{{ old('alt') }}" placeholder="Альтернативное название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="image">Фотография</label>
                <input class="input" type="file" name="image" id="image" placeholder="Обложка баннера">
            </f-field>
            <f-field class="grid gap-1">
                <label for="annotation">Аннотация</label>
                <textarea class="input" name="annotation" cols="30" rows="10">{{ old('annotation') }}</textarea>
            </f-field>
            <f-field class="grid gap-1">
                <label for="description">Описание</label>
                <textarea name="description" cols="30" rows="10" id="textarea">{{ old('description') }}</textarea>
            </f-field>
            <button class="btn btn-accent">Создать</button>
        </form>
        @include('components.ckeditor')
    </body>
</html>
