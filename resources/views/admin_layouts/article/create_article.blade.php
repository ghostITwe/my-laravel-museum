<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Создание статьи</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('articles.store') }}" method="post" enctype="multipart/form-data">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('articles.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Создание статьи</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            <f-field class="grid gap-1">
                <label for="title">Название статьи</label>
                <input class="input" type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Название статьи">
            </f-field>
            <f-field class="grid gap-1">
                <label for="alias">Псевдоним</label>
                <input class="input" type="text" name="alias" id="alias" value="{{ old('alias') }}" placeholder="Псевдоним">
            </f-field>
            <f-field class="grid gap-1">
                <label for="section">Секция</label>
                <select name="section" id="section">
                    <option value="" selected disabled>Выберите секцию</option>
                    @foreach($sections as $section)
                        <option value="{{ $section->id }}">{{ $section->title }}</option>
                    @endforeach
                </select>
            </f-field>
            <f-field class="grid gap-1">
                <label for="alt">Альтернативное название фотографии</label>
                <input class="input" type="text" name="alt" id="alt" value="{{ old('alt') }}" placeholder="Альтернативное название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="image">Фотография</label>
                <input class="input" type="file" name="image" id="image" placeholder="Обложка статьи">
            </f-field>
            <f-field class="grid gap-1">
                <label for="annotation">Аннотация</label>
                <textarea class="input" name="annotation" cols="30" rows="10" id="annotation">{{ old('annotation') }}</textarea>
            </f-field>
            <f-field class="grid gap-1">
                <label for="description">Описание</label>
                <textarea name="description" cols="30" rows="10" id="textarea">{{ old('description') }}</textarea>
            </f-field>
            <button class="btn btn-accent">Создать</button>
        </form>
        @include('components.ckeditor')
    </body>
</html>
