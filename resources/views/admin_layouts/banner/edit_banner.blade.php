<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Изменение баннера</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('banners.update', ['banner' => $banner]) }}" method="post" enctype="multipart/form-data">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('banners.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Изменение баннера</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            @method('PUT')
{{--            TODO: при изменение показывать текущую фотографию--}}
            <f-field class="grid gap-1">
                <label for="name">Название</label>
                <input class="input" type="text" name="name" id="name" value="{{ old('name') ?? $banner->name }}" placeholder="Название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="link">Ссылка</label>
                <input class="input" type="text" name="link" id="link" value="{{ old('link') ?? $banner->link }}" placeholder="Ссылка">
            </f-field>
            <f-field class="grid gap-1">
                <label for="alt">Альтернативное название фотографии</label>
                <input class="input" type="text" name="alt" id="alt" value="{{ old('alt') ?? $banner->image->alt }}" placeholder="Альтернативное название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="image">Фотография</label>
                <input class="input" type="file" name="image" id="image" placeholder="Обложка баннера">
            </f-field>
            <button class="btn btn-accent">Изменить</button>
        </form>
    </body>
</html>
