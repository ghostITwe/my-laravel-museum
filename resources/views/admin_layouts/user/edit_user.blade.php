<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Изменение пользователя</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('users.update', ['user' => $user]) }}" method="post">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('users.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Изменение пользователя</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            @method('PUT')
            <f-field class="grid gap-1">
                <label for="email">E-mail</label>
                <input class="input" type="text" name="email" id="email" value="{{ old('email') ?? $user->email}}" placeholder="Почта">
            </f-field>
            <f-field class="grid gap-1">
                <label for="login">Логин</label>
                <input class="input" type="text" name="login" id="login" value="{{ old('login') ?? $user->login }}" placeholder="Логин">
            </f-field>
            <f-field class="grid gap-1">
                <label for="role">Роль</label>
                <select name="role" id="role">
                    <option value="{{ $user->role }}">{{ $user->role }}</option>
                    <option value="null">Убрать права модератора</option>
                </select>
            </f-field>
{{--            FIXME: Подумать нужно ли менять пароль?--}}
{{--            <f-field class="grid gap-1">--}}
{{--                <label for="password">Пароль</label>--}}
{{--                <input class="input" type="password" name="password" id="password" placeholder="Пароль">--}}
{{--            </f-field>--}}
{{--            <f-field class="grid gap-1">--}}
{{--                <label for="password_confirm">Повторите пароль</label>--}}
{{--                <input class="input" type="password" name="password_confirmation" id="password_confirm" placeholder="Повторите пароль">--}}
{{--            </f-field>--}}
            <button class="btn btn-accent">Изменить</button>
        </form>
    </body>
</html>
