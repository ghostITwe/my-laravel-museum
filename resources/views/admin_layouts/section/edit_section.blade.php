<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Изменение секции</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body class="centered-page">
        <form class="centered-form p-4 sm:p-8" action="{{ route('sections.update', ['section' => $section]) }}" method="post" enctype="multipart/form-data">
            <header class="grid gap-2">
                <a class="link flex gap-1" href="{{ route('sections.index') }}">
                    @include('layouts.svg.left-arrow')
                    Назад
                </a>
                <h1 class="text-2xl sm:text-4xl text-center">Изменение секции</h1>
                <span class="line"></span>
            </header>
            @if ($errors->any())
                <div class="bg-red-500 border border-red-600 p-1 rounded text-white">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @csrf
            @method('PUT')
            <f-field class="grid gap-1">
                <label for="title">Название секции</label>
                <input class="input" type="text" name="title" id="title" value="{{ old('title') ?? $section->title }}" placeholder="Название секции">
            </f-field>
            <f-field class="grid gap-1">
                <label for="parent_id">Родительская секция</label>
                <select name="parent_id" id="parent_id">
                    <option value="null">Выберите родительскую секцию</option>
                    <option value="null">Убрать из родительской секции</option>
                    @foreach($sections as $section)
                        <option value="{{ $section->id }}">{{ $section->title }}</option>
                    @endforeach
                </select>
            </f-field>
            <f-field class="grid gap-1">
                <label for="alt">Альтернативное название фотографии</label>
                <input class="input" type="text" name="alt" id="alt" value="{{ old('alt') ?? $section->image->alt }}" placeholder="Альтернативное название">
            </f-field>
            <f-field class="grid gap-1">
                <label for="image">Фотография</label>
                <input class="input" type="file" name="image" id="image" placeholder="Обложка секции">
            </f-field>
            <f-field class="grid gap-1">
                <label for="description">Описание секции</label>
                <textarea class="input" name="description" cols="30" rows="10">{{ old('description') ?? $section->description }}</textarea>
            </f-field>
            <button class="btn btn-accent">Изменить</button>
        </form>
    </body>
</html>
