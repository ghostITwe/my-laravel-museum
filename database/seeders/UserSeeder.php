<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user[1] = new User();
        $user[1]->email = 'admin@admin.ru';
        $user[1]->login = 'admin';
        $user[1]->password = Hash::make('admin');
        $user[1]->role = 'admin';
        $user[1]->save();
    }
}
